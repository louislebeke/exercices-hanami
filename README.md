# Exercices de fafly

Projet à fork pour que vous puissiez faire les exercices.

## TODO List Project

### Specs

- Ajouter une tâche
- Pouvoir cocher ou décocher une tâche
- Pouvoir supprimer une tâche
- Pouvoir Mettre à jour une tâche
- Filtrer (avec une checkbox) pour afficher les tâches et les tâches faites avec leur date de réalisation.
- Créer une tâche aléatoire avec un boutton. La description de la tâche sera générée comme suit: prendre une chaîne aléatoire dans ACTIONS puis une autre dans OBJECT: ```description: `${ACTION} ${OBJECT}` ```

#### Plus d'infos

- Quand je coche une tâche cette tâche, celle-ci ne s'affiche plus (à moins que l'on souhaite affihcer les tâches faites)
- Les tâches sont triées par ordre d'insertion
- Toutes les tâches seront sauvegarder dans le localStorage
- Le code devra être uniquement en anglais

#### Constantes

```js
const ACTIONS = [
    'eat',
    'sleep',
    'sell',
    'buy',
    'destroy',
    'throw',
    'bury',
];


const OBJECTS = [
    'the banana',
    'the dog',
    'a fireman',
    'a dancing guy',
    'Station F',
    'the coffin',
];
```

## Rendu des exercices

Vous devrez faire une branche par Exercice.

## Exercices
A chaque fois que vous finissez un exercice je regarderais votre code pour vous faire des retours. Les exercices se suivent donc pas la peine d'essayer de faire des exercices en avance car sinon vous devrez recommencer certaines choses. Donc attendez que je vous dise tout est bon
avant de passer à la suite.

### Exercice 1: Créer ce projet

- Sur cette [adresse](https://gitlab.com/louislebeke/exercices-hanami): je clique sur "Fork", afin de copier le projet sur mon espace personnel
- Partie Bonus SSH: Générer une clé ssh afin de ne plus avoir besoin de taper mes identifiants de login/mot de passe pour réaliser des commandes sur gitlab, voir ce [lien](https://docs.gitlab.com/ee/ssh/README.html#generate-an-ssh-key-pair)
- A partir de ce site créer un nouveau projet React https://fr.reactjs.org via create-react-app
- Aller dans le dossier créer 
- Lié ce projet React a votre projet git: `git remote add origin https://gitlab.com/<mon_pseudo_gitlab>/exercices-hanami.git`
- Si le projet contient un fichier README.md supprimer le
- Ensuite Récupérer votre README: `git pull origin main`
- Et envoyez le setup de votre branche sur gitlab: `git push origin main`

### Exercice 2: GitLab c'est quoi

GitLab est un outil puissant et utile mais c'est bien de voir ces différentes fonctionnalités. 

* Ecrire une nouvelle Issue
* Faire une Merge Request
* Trouver le moyen de clore une issue dans un commit
* Créer un label Exercise 2
* Créer une liste dans Boards contenant les issues avec le label Exercise 2

### Exercice 3: Spec 1

### Exercice 4: Spec 2

### Exercice 5 : Spec 3

### Exercice 6: Et si on utilisait des librairies

- Utiliser la [Material-UI](https://material-ui.com) afin d'avoir un design basique
- Refaire vos composants en utilisant les composants Material-UI
- Se renseigner sur les linters et particulièrement sur [eslint/prettier](https://www.mkapica.com/react-eslint/)
- Installer une configuration eslint avec prettier et react en plugin

### Exercice 7: Spec 4

### Exercice 8: Spec 5

### Exercice 9: Spec 6

Si vous avez des questions n'hésitez pas. Le but est que vous progressiez :D
